package me.vinceh121.markovbot;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import io.vedder.ml.markov.consumer.TokenConsumer;
import io.vedder.ml.markov.tokens.Token;

public class SentenceConsumer extends TokenConsumer {
	private String str = "";
	private boolean addNewline = true;

	@Override
	public void consume(Collection<Token> collection) {
		List<String> punctuation = Arrays.asList(",", ";", ":", ".", "?", "!", "-");
		for (Token w : collection) {
			if (!punctuation.contains(w.toString())) {
				str += " ";
			}
			str += w.toString();
		}
		if (addNewline)
			str += "\n";
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public boolean isAddNewline() {
		return addNewline;
	}

	public void setAddNewline(boolean addNewline) {
		this.addNewline = addNewline;
	}
	
	

}