package me.vinceh121.markovbot;

import io.vedder.ml.markov.tokens.file.StringToken;

public class SenderToken extends StringToken {
	private String name;

	public SenderToken(String data, String name) {
		super(data);
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
