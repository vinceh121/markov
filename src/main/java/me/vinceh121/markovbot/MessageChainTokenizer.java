package me.vinceh121.markovbot;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import io.vedder.ml.markov.LookbackContainer;
import io.vedder.ml.markov.holder.TokenHolder;
import io.vedder.ml.markov.tokenizer.Tokenizer;
import io.vedder.ml.markov.tokens.Token;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageHistory;

public class MessageChainTokenizer extends Tokenizer {

	private static final Logger log = Logger.getLogger(MessageChainTokenizer.class);

	private final int LOOKBACK;
	private MessageChannel chan;
	private MarkovBot bot;

	static {
		log.setLevel(Level.WARN);
	}

	public MessageChainTokenizer(TokenHolder th, int lookback, MessageChannel chan, MarkovBot bot) {
		super(th);
		LOOKBACK = lookback;
		this.chan = chan;
		this.bot = bot;
	}

	@Override
	public void tokenize() {
		List<Token> toks = new ArrayList<Token>();
		MessageHistory hist = chan.getHistory();
		bot.retrieveLongHistory(hist);

		for (Message m : hist.getRetrievedHistory()) {
			if (!bot.isMarkovMessage(m)) {
				System.out.println("Adding text: " + m.getMember().getEffectiveName());
				SenderToken tok = new SenderToken(m.getAuthor().getId(), m.getMember().getEffectiveName());
				th.addToken(new LookbackContainer(1, toks), tok);
				toks.add(tok);
			}
		}

		System.out.println("Retrieved " + hist.size() + " messages");
	}

}
