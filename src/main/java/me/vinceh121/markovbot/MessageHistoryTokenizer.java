package me.vinceh121.markovbot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import io.vedder.ml.markov.LookbackContainer;
import io.vedder.ml.markov.holder.TokenHolder;
import io.vedder.ml.markov.tokenizer.Tokenizer;
import io.vedder.ml.markov.tokens.Token;
import io.vedder.ml.markov.tokens.file.DelimitToken;
import io.vedder.ml.markov.tokens.file.StringToken;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.exceptions.RateLimitedException;

public class MessageHistoryTokenizer extends Tokenizer {

	private static final Logger log = Logger.getLogger(MessageHistoryTokenizer.class);

	private final int LOOKBACK;
	private final Set<String> END_MARKS;
	private final Token DELIMIT_TOKEN = DelimitToken.getInstance();
	private MessageChannel chan;
	private Member mem;
	private MarkovBot bot;

	private List<String> listStrings = null;

	static {
		log.setLevel(Level.WARN);
	}

	public MessageHistoryTokenizer(TokenHolder th, int lookback, MessageChannel chan, Member mem, MarkovBot bot) {
		super(th);
		END_MARKS = new HashSet<String>(Arrays.asList(".", "?", "!"));
		LOOKBACK = lookback;
		this.chan = chan;
		this.mem = mem;
		this.bot = bot;
	}

	private List<String> getMessages() throws RateLimitedException {
		List<String> strs = new ArrayList<String>();
		MessageHistory hist = chan.getHistory();
		bot.retrieveLongHistory(hist);
		
		for (Message m : hist.getRetrievedHistory()) {
			if (m.getAuthor().getId().equals(mem.getId()) && !bot.isMarkovMessage(m)) {
//				System.out.println("Adding text: " + m.getContentStripped());
				strs.add(m.getContentStripped() + "\n");
			}
		}
		
		System.out.println("Retrieved " + strs.size() + " messages");
		
		return strs;
	}

	@Override
	public void tokenize() {
		try {
			this.listStrings = splitStrings(getMessages());
			addTokensToHolder();
		} catch (RateLimitedException e) {
			e.printStackTrace();
		}
	}

	private void addTokensToHolder() {
		List<Token> l = getTokens(this.listStrings);
		addTokenList(l);
	}

	private void addTokenList(List<Token> tokens) {
		log.info("Chunking " + tokens.size() + " tokens for user \"" + mem.getAsMention() + "\"...\n");
		for (int wordIndex = LOOKBACK; wordIndex < tokens.size() - 1; wordIndex++) {

			// List for the lookback
			List<Token> lookBackList = new ArrayList<Token>(this.LOOKBACK);

			Token t = null;

			// loop adds lists to ensure that lookback lists of size 1 to size
			// "lookBack" are added to the lookbackList
			chunkLoop: for (int lookBackCount = 0; lookBackCount < this.LOOKBACK; lookBackCount++) {
				t = tokens.get(wordIndex - lookBackCount);
				lookBackList.add(0, t);

				// constructor call is to copy lookBackList
				th.addToken(new LookbackContainer(this.LOOKBACK, lookBackList), tokens.get(wordIndex + 1));

				// if lookback hits delimiter token, stop
				if (t == DELIMIT_TOKEN) {
					break chunkLoop;
				}
			}
		}
	}

	private List<Token> getTokens(List<String> listStrings) {
		List<Token> tokenList = new LinkedList<Token>();
		tokenList.add(DELIMIT_TOKEN);

		for (String s : listStrings) {
			tokenList.add(new StringToken(s));
			if (END_MARKS.contains(s)) {
				tokenList.add(DELIMIT_TOKEN);
			}
		}

		// check to see if ends with delimiter token
		if (tokenList.get(tokenList.size() - 1) != DELIMIT_TOKEN) {
			tokenList.add(DELIMIT_TOKEN);
		}
		return tokenList;
	}

	private List<String> splitStrings(List<String> lines) {
		// Regex from:
		// http://stackoverflow.com/questions/24222730/split-a-string-and-separate-by-punctuation-and-whitespace
		List<String> splits = new ArrayList<String>();
		List<String> temp;
		for (String s : lines) {
			temp = Arrays.asList(s.replaceAll("  ", " ")
					.split("\\s+|(?=\\W\\p{Punct}|\\p{Punct}\\W)|(?<=\\W\\p{Punct}|\\p{Punct}\\W})"));
			for (String t : temp) {
				if (t != "" && !t.isEmpty()) {
					splits.add(t);
				}
			}
		}
		return splits;
	}

}
