package me.vinceh121.markovbot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import io.vedder.ml.markov.generator.Generator;
import io.vedder.ml.markov.generator.file.FileGenerator;
import io.vedder.ml.markov.holder.MapTokenHolder;
import io.vedder.ml.markov.holder.TokenHolder;
import io.vedder.ml.markov.threading.JobManager;
import io.vedder.ml.markov.tokens.Token;
import me.vinceh121.markovbot.commands.CommandConversation;
import me.vinceh121.markovbot.commands.CommandDelete;
import me.vinceh121.markovbot.commands.CommandFuckYou;
import me.vinceh121.markovbot.commands.CommandHelp;
import me.vinceh121.markovbot.commands.CommandNickname;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDA.Status;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.MentionType;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class MarkovBot extends ListenerAdapter {
	private static Logger LOG = Logger.getLogger("MARKOV");

	private static final List<String> allowedUsers = Arrays.asList("340473152259751936", "94762492923748352",
			"495170318483193857"), adminUsers = Arrays.asList("340473152259751936");

	private static final int LOOKBACK = 1;
	private static final int MAPSIZE = 10000;
	public static final int MSG_LIMIT = 100;

	private static final int RATELIMIT = 30000;

	private JDA bot;

	private Hashtable<String, Long> reqTimes = new Hashtable<String, Long>();
	private Hashtable<String, Command> commandReg = new Hashtable<String, Command>();

	public static void main(String[] args) {
		Logger.getLogger(JobManager.class).setLevel(Level.WARN);
		MarkovBot instance = new MarkovBot(args[0]);
		instance.start();
	}

	public MarkovBot(String token) {
		registerCommands();
		try {
			bot = new JDABuilder(token).build();
		} catch (LoginException e) {
			LOG.error("Failed to login: " + e.toString() + "\n");
			System.exit(-1);
		}
		LOG.info("Waiting to be connected...\n");
		try {
			bot.awaitStatus(Status.CONNECTED);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		LOG.info("Connected!\n");
	}

	public Collection<Command> getCommands() {
		return commandReg.values();
	}

	private void registerCommands() {
		regCmd(new CommandFuckYou(this));
		regCmd(new CommandHelp(this));
		regCmd(new CommandConversation(this));
		regCmd(new CommandNickname(this));
		regCmd(new CommandDelete(this));
	}

	private void regCmd(Command cmd) {
		this.commandReg.put(cmd.getName(), cmd);
	}

	public void start() {
		LOG.info("Starting\n");
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			public void run() {
				bot.shutdownNow();
			}
		}));
		bot.addEventListener(this);
	}

	public JDA getJDA() {
		return bot;
	}
	
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		if (event.getAuthor().getId().equals(bot.getSelfUser().getId())) // Don't do shit if it's our message
			return;
		if (!event.getMessage().isMentioned(bot.getSelfUser(), MentionType.USER)) // Don't do shit if we aren't
																					// mentioned
			return;
		if (event.getMessage().getMentionedUsers().get(0).getIdLong() != bot.getSelfUser().getIdLong()) // Don't do shit
																										// if we aren't
																										// the first
																										// mentioned
			return;

		reqTimes.put(event.getAuthor().getId(), System.currentTimeMillis()); // Reset rate limiting timer

		if (reqTimes.get(event.getAuthor().getId()) != null
				&& System.currentTimeMillis() - reqTimes.get(event.getAuthor().getId()) <= RATELIMIT
				&& !adminUsers.contains(event.getAuthor().getId())) {
			LOG.warn("User " + event.getAuthor().getAsMention() + " is being rate limited\n");
			event.getChannel()
					.sendMessage(
							"You're doing this too much. Wait " + (RATELIMIT / 1000) + "s or gulag will be your home.")
					.submit();
			return;
		}

		if (!allowedUsers.contains(event.getMember().getId())) {
			LOG.info("User " + event.getAuthor().getAsMention() + " tried to use bot but doesn't have rights\n");
			event.getChannel().sendMessage("Sorry comrad, you're not allowed to use the Markov bot.\n"
					+ "Keep asking me and you'll know Siberia").submit();
			return;
		}

		System.out.println(bot.getSelfUser().getAsMention());

		if (event.getMessage().getContentRaw().replace("!", "").equals(bot.getSelfUser().getAsMention())) { // Only
			// markov
			// mention

			// So request on self
			processRequest(event.getChannel(), event.getMember(), event.getMember());
		} else if (event.getMessage().getMentionedMembers().size() == 2) {
			processRequest(event.getChannel(), event.getMessage().getMentionedMembers().get(1), event.getMember());
		} else {
			executeCommand(event);
		}
	}

	private void executeCommand(MessageReceivedEvent event) {
		Command cmd = getCommand(
				event.getMessage().getContentRaw().substring(bot.getSelfUser().getAsMention().length() + 2));
		if (cmd != null) {
			LOG.info(event.getAuthor().getAsMention() + " executed command " + event.getMessage().getContentRaw());
			cmd.processCommand(event.getMessage().getContentRaw().substring(cmd.getName().length()).split(" "),
					event.getChannel(), event.getMember());
		} else {
			LOG.info("Could not understand request '" + event.getMessage().getContentRaw() + "' by user "
					+ event.getAuthor().getAsMention() + "\n");
			event.getChannel().sendMessage("I could not understand your request comrad").submit();
		}
	}

	private Command getCommand(String str) {
		if (str == null || str.isEmpty())
			return null;
		for (String name : commandReg.keySet()) {
			if (str.startsWith(name))
				return commandReg.get(name);
		}
		return null;
	}

	public List<String> generateConvo(MessageChannel chan) {
		List<String> ids = new ArrayList<String>();

		TokenHolder tm = new MapTokenHolder(MAPSIZE);
		JobManager jm = new JobManager();

		MessageChainTokenizer tok = new MessageChainTokenizer(tm, LOOKBACK, chan, this);
		jm.addTokenizer(tok);
		jm.runAll();

		FileGenerator g = new FileGenerator(tm, LOOKBACK);

		List<Collection<Token>> tokensCollections = new LinkedList<Collection<Token>>();

		tokensCollections.add(g.generateTokenList());
		tokensCollections.add(g.generateLazyTokenList());

		SentenceConsumer tc = new SentenceConsumer();

		for (Collection<Token> l : tokensCollections) {
			tc.consume(l);
		}

		System.out.println(tc.getStr());

		return ids;
	}

	public String generateMessageForUser(MessageChannel chan, Member mem) {
		TokenHolder tm = new MapTokenHolder(MAPSIZE);
		JobManager jm = new JobManager();

		MessageHistoryTokenizer tok = new MessageHistoryTokenizer(tm, LOOKBACK, chan, mem, this);
		jm.addTokenizer(tok);

		jm.runAll();
//		System.out.println(tm.toString());
		Generator g = new FileGenerator(tm, LOOKBACK);

		List<Collection<Token>> tokensCollections = new LinkedList<Collection<Token>>();

//		System.out.println(tm.toString());

		// Creates Lists of tokens
//		for (int i = 0; i < number / 2; i++) {
		tokensCollections.add(g.generateTokenList());
//		}

		// Creates lazy collections of tokens
//		for (int i = 0; i < (number / 2 + number % 2); i++) {
		tokensCollections.add(g.generateLazyTokenList());
//		}

		// Consumer consumes both types of collections
//		System.out.println("Printing " + tokensCollections.size() + " Tokens...\n" + "===============\n");

		SentenceConsumer tc = new SentenceConsumer();

		for (Collection<Token> l : tokensCollections) {
			tc.consume(l);
		}
		return tc.getStr();
	}

	public void processRequest(MessageChannel chan, Member target, Member requester) {
		LOG.info("User " + target.getAsMention() + "'s request to use the bot as been granted\n");
		chan.sendMessage("Да, mister Markov will be processing your request soon **☭**").submit();
		try {
			String msg = generateMessageForUser(chan, target);
			chan.sendMessage(target.getEffectiveName() + ":\n>>> " + msg).submit();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (e.getMessage().equals("Unable to find match to given input"))
				chan.sendMessage(
						"We Хave not been able to complete your request as this user did not send enough messages")
						.submit();
		}
	}

	public void updateNickname(Guild guild, MessageChannel callback) {
		TokenHolder tm = new MapTokenHolder(MAPSIZE);
		JobManager jm = new JobManager();

		GuildNicksTokenizer tok = new GuildNicksTokenizer(tm, LOOKBACK, guild);
		jm.addTokenizer(tok);

		jm.runAll();

		Generator g = new FileGenerator(tm, LOOKBACK);

		List<Collection<Token>> tokensCollections = new LinkedList<Collection<Token>>();
		try {
			tokensCollections.add(g.generateTokenList());
			tokensCollections.add(g.generateLazyTokenList());
		} catch (RuntimeException e) {
			if (e.getMessage().equals("Unable to find match to given input"))
				callback.sendMessage("There is not enough nicknames in this server for me to generate one.").submit();
			return;
		}
		SentenceConsumer tc = new SentenceConsumer();

		for (Collection<Token> l : tokensCollections) {
			tc.consume(l);
		}

		callback.sendMessage("My new username will be *" + tc.getStr() + "*").submit();

		guild.modifyNickname(guild.getMember(bot.getSelfUser()), tc.getStr());
	}

	public void retrieveLongHistory(MessageHistory hist) {
		for (int i = 0; i < 10; i++)
			hist.retrievePast(100).complete();
	}

	public boolean isMarkovMessage(Message msg) {
		return msg.getAuthor().getId().equals(bot.getSelfUser().getId())
				|| msg.isMentioned(bot.getSelfUser(), MentionType.USER);
	}

}
