package me.vinceh121.markovbot;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;

public abstract class Command {
	protected MarkovBot instance;

	public Command(MarkovBot instance) {
		this.instance = instance;
	}

	public abstract String getName();

	public abstract String getHelp();

	public abstract void processCommand(String[] args, MessageChannel channel, Member caller);
}
