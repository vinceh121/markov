package me.vinceh121.markovbot.commands;

import me.vinceh121.markovbot.Command;
import me.vinceh121.markovbot.MarkovBot;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;

public class CommandConversation extends Command {

	public CommandConversation(MarkovBot instance) {
		super(instance);
	}

	@Override
	public String getName() {
		return "conversation";
	}

	@Override
	public String getHelp() {
		return "Generates a conversion";
	}

	@Override
	public void processCommand(String[] args, MessageChannel channel, Member caller) {
		instance.generateConvo(channel);
	}

}
