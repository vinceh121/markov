package me.vinceh121.markovbot.commands;

import me.vinceh121.markovbot.Command;
import me.vinceh121.markovbot.MarkovBot;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;

public class CommandHelp extends Command {
	private static String helpPage;

	public CommandHelp(MarkovBot instance) {
		super(instance);
	}

	@Override
	public String getName() {
		return "help";
	}

	@Override
	public String getHelp() {
		return "Shows the help page";
	}

	@Override
	public void processCommand(String[] args, MessageChannel channel, Member caller) {
		if (helpPage == null) {
			final StringBuilder sb = new StringBuilder();
			sb.append("Markov Bot Help:\n");
			for (Command c : instance.getCommands()) {
				sb.append(c.getName() + "\t" + c.getHelp() + "\n");
			}
			helpPage = sb.toString();
		}
		channel.sendMessage(helpPage).submit();
	}

}
