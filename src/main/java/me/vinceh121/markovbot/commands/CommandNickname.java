package me.vinceh121.markovbot.commands;

import me.vinceh121.markovbot.Command;
import me.vinceh121.markovbot.MarkovBot;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;

public class CommandNickname extends Command {

	public CommandNickname(MarkovBot instance) {
		super(instance);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		return "nickname";
	}

	@Override
	public String getHelp() {
		return "Updates the bot's nickname for this guild";
	}

	@Override
	public void processCommand(String[] args, MessageChannel channel, Member caller) {
		instance.updateNickname(caller.getGuild(), channel);
	}

}
