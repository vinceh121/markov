package me.vinceh121.markovbot.commands;

import me.vinceh121.markovbot.Command;
import me.vinceh121.markovbot.MarkovBot;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;

public class CommandDelete extends Command {

	public CommandDelete(MarkovBot instance) {
		super(instance);
	}

	@Override
	public String getName() {
		return "delete";
	}

	@Override
	public String getHelp() {
		return "Deletes the last 100 Markov related messages";
	}

	@Override
	public void processCommand(String[] args, MessageChannel channel, Member caller) {
		for (Message m : channel.getHistory().retrievePast(100).complete()) {
			if (instance.isMarkovMessage(m))
				m.delete().complete();
		}
	}

}
